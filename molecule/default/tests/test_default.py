import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('jms2rdb')


def test_jms2rdb_index(host):
    cmd = host.command('curl http://localhost:4913/main')
    assert '<title>JMS-to-RDB Tool</title>' in cmd.stdout
